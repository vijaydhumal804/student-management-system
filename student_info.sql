/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.29 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `student_info` (
	`student_id` int (11),
	`student_name` varchar (60),
	`student_stream` varchar (60),
	`student_percentage` double ,
	`student_course` varchar (60)
); 
insert into `student_info` (`student_id`, `student_name`, `student_stream`, `student_percentage`, `student_course`) values('1','Vijay Dhumal','IT','84.17','BTECH');
insert into `student_info` (`student_id`, `student_name`, `student_stream`, `student_percentage`, `student_course`) values('2','Ayushi Patel','CO','80','Btech');
insert into `student_info` (`student_id`, `student_name`, `student_stream`, `student_percentage`, `student_course`) values('3','Mallika Singh','ECE','75.25','Btech');
insert into `student_info` (`student_id`, `student_name`, `student_stream`, `student_percentage`, `student_course`) values('4','Sumedh Mugdhalakar','IT','85','Btech');
insert into `student_info` (`student_id`, `student_name`, `student_stream`, `student_percentage`, `student_course`) values('5','Riva Arora','IT','95','Btech');
insert into `student_info` (`student_id`, `student_name`, `student_stream`, `student_percentage`, `student_course`) values('6','Vijay Dhumal','IT','99.99','Btech');
insert into `student_info` (`student_id`, `student_name`, `student_stream`, `student_percentage`, `student_course`) values('7','Vijay','ECE','85.8','Btech');
