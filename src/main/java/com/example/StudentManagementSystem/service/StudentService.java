package com.example.StudentManagementSystem.service;

import com.example.StudentManagementSystem.domain.Student;
import com.example.StudentManagementSystem.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentService {
    @Autowired
    private StudentRepository repository;
    List<Student> studentList;

    // Add New Student
    public void addStudent(Student student)
    {
        repository.save(student);
    }
    // get All Students
    public List<Student> getAllStudents(){
        studentList =repository.findAll();
        return studentList;
    }

    // Display Student By Id
    public Student getStudentById(int id){
        Student s=repository.findById(id).orElse(null);
        return s;
    }

    // Update Existing Student Details by Id

    public void updateStudent(Student s){
        repository.save(s);
    }

    // Delete Existing Student by Id
    public void deleteStudent(int id){
        repository.deleteById(id);

    }


}
