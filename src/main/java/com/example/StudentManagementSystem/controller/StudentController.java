package com.example.StudentManagementSystem.controller;

import com.example.StudentManagementSystem.domain.Student;
import com.example.StudentManagementSystem.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StudentController {
    @Autowired
    private StudentService service;

    @GetMapping("/addstudentform")
    public String getStudentForm(Model model){
        Student student=new Student();
        model.addAttribute("students",student);
        return "addstudentform";
    }

    @GetMapping("/addstudent")
    public String addStudents(Student student){
        service.addStudent(student);
        return "redirect:/";
    }

    @GetMapping("/")
    public String getAllStudents(Model model){
        model.addAttribute("students",service.getAllStudents());
        return "studenthome";
    }

    @GetMapping("updatestudent/{id}")
    public String getStudentUpdateForm(@PathVariable(name = "id") int id, Model model){
        Student student = new Student();
        model.addAttribute("students",service.getStudentById(id));
        return "updatestudentform";
    }

    @PostMapping("modifystudent")
    public String modifyStudent(Student student){
        service.updateStudent(student);
        return "redirect:/";
    }

    @GetMapping("deletestudent/{id}")
    public String deleteStudent(@PathVariable(name = "id") int id, Model model){
        service.deleteStudent(id);
        return "redirect:/";
    }

}
